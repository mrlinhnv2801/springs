package com.example.zipkinapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import zipkin.server.EnableZipkinServer;

@SuppressWarnings("deprecation")
@SpringBootApplication
@EnableZipkinServer
public class ZipkinappApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZipkinappApplication.class, args);
	}
	

}
